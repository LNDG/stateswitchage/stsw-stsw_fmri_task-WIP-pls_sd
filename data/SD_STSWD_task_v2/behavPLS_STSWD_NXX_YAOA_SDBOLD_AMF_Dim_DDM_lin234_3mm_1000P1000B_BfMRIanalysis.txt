%------------------------------------------------------------------------

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%  Result File Name Start  %
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Note: Result file must be listed first, and must follow the file
%	 name format of xxxx_yyyyresult.mat, where xxxx stands for
%	 "any result file name prefix" and yyyy stands for the name
%	 of PLS module (either PET ERP fMRI BfMRI STRUCT or SmallFC).
%	 File name is case sensitive on Unix or Linux computers.

result_file	behavPLS_STSWD_NXX_YAOA_SDBOLD_AMF_Dim_DDM_lin234_3mm_1000P1000B_BfMRIresult.mat

	%%%%%%%%%%%%%%%%%%%%%%%%%%
	%  Result File Name End  %
	%%%%%%%%%%%%%%%%%%%%%%%%%%

%------------------------------------------------------------------------

	%%%%%%%%%%%%%%%%%%%%%%%%%
	%  Group Section Start  %
	%%%%%%%%%%%%%%%%%%%%%%%%%

group_files SD_1117_v6_BfMRIsessiondata.mat SD_1118_v6_BfMRIsessiondata.mat SD_1120_v6_BfMRIsessiondata.mat SD_1126_v6_BfMRIsessiondata.mat SD_1131_v6_BfMRIsessiondata.mat SD_1132_v6_BfMRIsessiondata.mat SD_1135_v6_BfMRIsessiondata.mat SD_1136_v6_BfMRIsessiondata.mat SD_1160_v6_BfMRIsessiondata.mat SD_1164_v6_BfMRIsessiondata.mat SD_1167_v6_BfMRIsessiondata.mat SD_1169_v6_BfMRIsessiondata.mat SD_1172_v6_BfMRIsessiondata.mat SD_1173_v6_BfMRIsessiondata.mat SD_1178_v6_BfMRIsessiondata.mat SD_1182_v6_BfMRIsessiondata.mat SD_1215_v6_BfMRIsessiondata.mat SD_1216_v6_BfMRIsessiondata.mat SD_1219_v6_BfMRIsessiondata.mat SD_1223_v6_BfMRIsessiondata.mat SD_1227_v6_BfMRIsessiondata.mat SD_1233_v6_BfMRIsessiondata.mat SD_1234_v6_BfMRIsessiondata.mat SD_1237_v6_BfMRIsessiondata.mat SD_1239_v6_BfMRIsessiondata.mat SD_1240_v6_BfMRIsessiondata.mat SD_1243_v6_BfMRIsessiondata.mat SD_1245_v6_BfMRIsessiondata.mat SD_1250_v6_BfMRIsessiondata.mat SD_1257_v6_BfMRIsessiondata.mat SD_1261_v6_BfMRIsessiondata.mat SD_1265_v6_BfMRIsessiondata.mat SD_1266_v6_BfMRIsessiondata.mat SD_1268_v6_BfMRIsessiondata.mat SD_1270_v6_BfMRIsessiondata.mat SD_1276_v6_BfMRIsessiondata.mat SD_1281_v6_BfMRIsessiondata.mat
group_files SD_2104_v6_BfMRIsessiondata.mat SD_2107_v6_BfMRIsessiondata.mat SD_2108_v6_BfMRIsessiondata.mat SD_2112_v6_BfMRIsessiondata.mat SD_2118_v6_BfMRIsessiondata.mat SD_2120_v6_BfMRIsessiondata.mat SD_2121_v6_BfMRIsessiondata.mat SD_2123_v6_BfMRIsessiondata.mat SD_2125_v6_BfMRIsessiondata.mat SD_2129_v6_BfMRIsessiondata.mat SD_2130_v6_BfMRIsessiondata.mat SD_2132_v6_BfMRIsessiondata.mat SD_2133_v6_BfMRIsessiondata.mat SD_2134_v6_BfMRIsessiondata.mat SD_2135_v6_BfMRIsessiondata.mat SD_2139_v6_BfMRIsessiondata.mat SD_2140_v6_BfMRIsessiondata.mat SD_2145_v6_BfMRIsessiondata.mat SD_2147_v6_BfMRIsessiondata.mat SD_2149_v6_BfMRIsessiondata.mat SD_2157_v6_BfMRIsessiondata.mat SD_2160_v6_BfMRIsessiondata.mat SD_2201_v6_BfMRIsessiondata.mat SD_2202_v6_BfMRIsessiondata.mat SD_2203_v6_BfMRIsessiondata.mat SD_2205_v6_BfMRIsessiondata.mat SD_2206_v6_BfMRIsessiondata.mat SD_2209_v6_BfMRIsessiondata.mat SD_2210_v6_BfMRIsessiondata.mat SD_2211_v6_BfMRIsessiondata.mat SD_2213_v6_BfMRIsessiondata.mat SD_2214_v6_BfMRIsessiondata.mat SD_2215_v6_BfMRIsessiondata.mat SD_2216_v6_BfMRIsessiondata.mat SD_2217_v6_BfMRIsessiondata.mat SD_2219_v6_BfMRIsessiondata.mat SD_2222_v6_BfMRIsessiondata.mat SD_2224_v6_BfMRIsessiondata.mat SD_2226_v6_BfMRIsessiondata.mat SD_2227_v6_BfMRIsessiondata.mat SD_2236_v6_BfMRIsessiondata.mat SD_2238_v6_BfMRIsessiondata.mat SD_2241_v6_BfMRIsessiondata.mat SD_2244_v6_BfMRIsessiondata.mat SD_2246_v6_BfMRIsessiondata.mat SD_2248_v6_BfMRIsessiondata.mat SD_2250_v6_BfMRIsessiondata.mat SD_2251_v6_BfMRIsessiondata.mat SD_2252_v6_BfMRIsessiondata.mat SD_2258_v6_BfMRIsessiondata.mat SD_2261_v6_BfMRIsessiondata.mat

% ... following above pattern for more groups

	%%%%%%%%%%%%%%%%%%%%%%%
	%  Group Section End  %
	%%%%%%%%%%%%%%%%%%%%%%%

%------------------------------------------------------------------------

	%%%%%%%%%%%%%%%%%%%%%%%
	%  PLS Section Start  %
	%%%%%%%%%%%%%%%%%%%%%%%

%  Notes:
%    1. Mean-Centering PLS
%    2. Non-Rotated Task PLS (please also fill out contrast data below)
%    3. Regular Behav PLS (please also fill out behavior data & name below)
%    4. Multiblock PLS (please also fill out behavior data & name below)
%    5. Non-Rotated Behav PLS (please also fill out contrast data and
%	behavior data & name below)
%    6. Non-Rotated Multiblock PLS (please also fill out contrast data and
%	behavior data & name below)

pls		3		% PLS Option (between 1 to 6, see above notes)

%  Mean-Centering Type:
%    0. Remove group condition means from conditon means within each group
%    1. Remove grand condition means from each group condition mean
%    2. Remove grand mean over all subjects and conditions
%    3. Remove all main effects by subtracting condition and group means

mean_type	0		% Mean-Centering Type (between 0 to 3, see above)

%  Correlation Mode:
%    0. Pearson correlation
%    2. covaraince
%    4. cosine angle
%    6. dot product

cormode		0		% Correlation Mode (can be 0,2,4,6, see above)

num_perm	1000		% Number of Permutation
num_split	0		% Natasha Perm Split Half
num_boot	1000		% Number of Bootstrap
boot_type	strat		% Either strat or nonstrat bootstrap type
clim		95		% Confidence Level for Behavior PLS
save_data	0		% Set to 1 to save stacked datamat

	%%%%%%%%%%%%%%%%%%%%%
	%  PLS Section End  %
	%%%%%%%%%%%%%%%%%%%%%

%------------------------------------------------------------------------

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%  Condition Selection Start  %
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Notes: If you don't need to deselect conditions, just leave
%  "selected_cond" and "selected_bcond" to be commented.

%  First put k number of 1 after "selected_cond" keyword, where k is the
%  number of conditions in sessiondata file. Then, replace with 0 for
%  those conditions that you would like to deselect for any case except
%  behavior block of multiblock PLS. e.g. If you have 3 conditions in
%  sessiondata file, and you would like to deselect the 2nd condition,
%  then you should enter 1 0 1 after selected_cond.
%
 selected_cond	0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%  Condition Selection End  %
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%------------------------------------------------------------------------

%------------------------------------------------------------------------


	%%%%%%%%%%%%%%%%%%%%%%%%%
	%  Behavior Data Start  %
	%%%%%%%%%%%%%%%%%%%%%%%%%

%  Notes: only list selected conditions (selected_cond)

behavior_data    -1.183415e-02 2.709559e-01 5.895564e-02 1.972099e+00 8.304242e-02 1.037555e+00 -5.000000e-01 
behavior_data    -9.812138e-02 2.609860e-01 3.106251e-02 1.568664e+00 -1.875070e-01 7.707688e-01 -1.200000e+01 
behavior_data    3.328973e+00 2.211513e-01 9.443519e-02 1.694563e+00 7.628945e-02 1.159456e+00 -1.500000e+00 
behavior_data    -6.509675e-01 2.577518e-01 5.164729e-02 1.198993e+00 2.639587e-02 9.805455e-01 0 
behavior_data    5.929773e-01 2.891378e-01 6.317978e-02 2.064625e+00 -2.331292e-01 6.732760e-01 -8.000000e+00 
behavior_data    8.107784e-02 3.410162e-01 1.474104e-01 2.166183e+00 -1.230933e-01 1.015256e+00 8.500000e+00 
behavior_data    5.201196e-01 3.079806e-01 3.969162e-02 2.656411e+00 3.275615e-02 7.299831e-01 2.500000e+00 
behavior_data    1.813089e-01 2.359764e-01 -9.344910e-03 1.244043e+00 1.491280e-01 1.481402e+00 -5.000000e+00 
behavior_data    6.807707e-01 2.927191e-01 8.715095e-03 1.754924e+00 -6.490525e-02 1.179008e+00 -2.500000e+00 
behavior_data    4.476990e-01 2.331051e-01 6.513624e-02 1.704409e+00 -2.033850e-02 1.045834e+00 8.000000e+00 
behavior_data    3.418773e-01 2.813414e-01 8.905143e-02 1.683241e+00 -5.097073e-02 1.239479e+00 3.500000e+00 
behavior_data    -1.556659e+00 2.201740e-01 9.241440e-03 2.004700e+00 -1.465433e-01 1.333010e+00 -7.000000e+00 
behavior_data    1.918783e+00 3.278128e-01 3.128093e-02 1.670319e+00 -1.833729e-01 1.090998e+00 -1.100000e+01 
behavior_data    -1.181192e+00 3.950941e-01 6.575476e-03 5.678273e-01 -2.111632e-01 1.102714e+00 7.500000e+00 
behavior_data    -2.701595e-01 3.025683e-01 1.354625e-01 1.438073e+00 -6.511063e-04 1.194443e+00 -1.600000e+01 
behavior_data    6.916868e-01 2.739581e-01 6.812154e-02 2.122316e+00 -6.569567e-02 1.192633e+00 -5.500000e+00 
behavior_data    1.102238e+00 3.243115e-01 1.363447e-02 1.813427e+00 -1.815550e-01 8.756570e-01 1.300000e+01 
behavior_data    5.941402e-01 2.574110e-01 5.667128e-02 1.702563e+00 -1.166412e-01 7.371464e-01 1.150000e+01 
behavior_data    -3.331344e-01 2.644845e-01 9.932959e-02 2.679888e+00 -1.550952e-01 8.896239e-01 -8.500000e+00 
behavior_data    1.538725e+00 2.838014e-01 5.100285e-02 1.407006e+00 -5.813458e-02 1.189423e+00 -9.500000e+00 
behavior_data    -1.715909e+00 3.331436e-01 2.576796e-02 1.110986e+00 -1.126868e-01 1.410961e+00 1.100000e+01 
behavior_data    -5.795789e-01 2.821583e-01 -1.567284e-02 2.507317e+00 -1.999991e-01 6.575321e-01 2.000000e+00 
behavior_data    2.581743e-01 2.436056e-01 4.469298e-02 1.758532e+00 -5.181512e-02 8.030292e-01 2.850000e+01 
behavior_data    1.469529e+00 2.868731e-01 4.872010e-02 1.576246e+00 -2.465314e-02 9.840882e-01 1.400000e+01 
behavior_data    6.337355e-01 2.807900e-01 2.455915e-02 2.386866e+00 -1.682277e-01 7.819911e-01 5.500000e+00 
behavior_data    -5.364063e-01 2.691130e-01 8.922105e-02 2.011900e+00 6.312454e-02 1.085267e+00 -7.500000e+00 
behavior_data    1.195512e+00 3.573002e-01 -8.959447e-04 1.610041e+00 -1.721144e-01 9.677838e-01 4.000000e+00 
behavior_data    1.081528e+00 2.643481e-01 6.398304e-02 1.969477e+00 4.026283e-02 1.138172e+00 -1.850000e+01 
behavior_data    2.137823e+00 2.327507e-01 1.104509e-01 2.483006e+00 1.862670e-02 8.813642e-01 9.000000e+00 
behavior_data    -7.635073e-01 3.099204e-01 3.243204e-02 1.922927e+00 -1.762653e-01 9.210348e-01 -6.500000e+00 
behavior_data    1.794022e+00 2.789667e-01 4.679872e-02 2.517712e+00 -1.749371e-01 8.876174e-01 7.000000e+00 
behavior_data    -4.654457e-01 2.546074e-01 3.058275e-02 1.670364e+00 -3.255260e-01 9.015381e-01 -1.900000e+01 
behavior_data    1.210071e+00 2.323866e-01 4.981283e-02 2.732828e+00 -2.276007e-01 8.836132e-01 2.000000e+00 
behavior_data    -6.250032e-01 2.404574e-01 9.713108e-02 1.520466e+00 -3.168602e-02 1.113262e+00 -1.400000e+01 
behavior_data    -3.285427e-03 2.278760e-01 3.542141e-02 1.734419e+00 6.846171e-02 1.065923e+00 1.400000e+01 
behavior_data    -1.964015e+00 3.025762e-01 -3.352830e-03 1.853643e+00 -2.395376e-01 7.913388e-01 6.500000e+00 
behavior_data    -1.077273e+00 3.002027e-01 8.466373e-03 2.362227e+00 -9.821052e-02 1.161604e+00 2.500000e+00 

behavior_data    -8.847552e-01 2.962951e-01 -1.591376e-02 6.855113e-01 1.101540e-01 8.929938e-01 1.500000e+01 
behavior_data    -3.066077e-01 2.428830e-01 1.072543e-01 1.111007e+00 2.297852e-02 1.279054e+00 -1.050000e+01 
behavior_data    -1.149757e-01 3.369540e-01 1.252117e-02 1.319006e+00 1.957559e-02 1.039997e+00 2.000000e+00 
behavior_data    -8.523938e-01 2.667883e-01 -3.469639e-03 -7.444507e-02 8.219651e-02 1.700288e+00 -4.500000e+00 
behavior_data    -2.364004e+00 2.749491e-01 -1.883825e-02 1.035208e+00 -5.580212e-02 1.288399e+00 -1.550000e+01 
behavior_data    -9.148322e-01 2.403289e-01 -2.093525e-02 5.503630e-01 -1.674423e-02 1.643361e+00 1.900000e+01 
behavior_data    2.441841e-01 3.568992e-01 -4.816460e-02 1.479911e+00 -5.325453e-02 1.358833e+00 -8.500000e+00 
behavior_data    2.764090e-01 3.595648e-01 5.290780e-02 6.134226e-01 1.347150e-02 1.235066e+00 -1.450000e+01 
behavior_data    4.911822e-02 3.366364e-01 4.001388e-02 9.610047e-01 -1.280327e-01 9.747409e-01 7.000000e+00 
behavior_data    3.157215e-01 3.877191e-01 2.448668e-02 9.545454e-01 -1.732776e-01 1.305930e+00 8.000000e+00 
behavior_data    7.732182e-03 2.957362e-01 1.038195e-01 1.202673e+00 -9.864240e-02 1.635233e+00 1.500000e+00 
behavior_data    -7.093699e-01 2.975151e-01 3.717602e-04 1.706354e+00 -2.394375e-01 6.056157e-01 0 
behavior_data    6.162264e-01 3.534955e-01 2.378338e-02 1.353661e+00 2.391657e-03 8.953297e-01 4.500000e+00 
behavior_data    9.495112e-03 3.850094e-01 1.068667e-02 1.145760e+00 -2.589731e-02 1.005454e+00 -4.500000e+00 
behavior_data    -7.070675e-02 3.981832e-01 4.225117e-02 1.586717e+00 -1.177332e-01 7.178591e-01 -7.000000e+00 
behavior_data    6.558189e-01 2.873278e-01 6.709917e-02 1.118126e+00 2.683627e-02 9.290324e-01 -4.500000e+00 
behavior_data    -8.098736e-01 3.600828e-01 -6.732162e-03 9.932699e-01 -8.568453e-03 1.103419e+00 5.000000e-01 
behavior_data    -1.228320e+00 2.496257e-01 1.036425e-03 8.340683e-01 -3.349259e-02 6.435573e-01 -5.000000e-01 
behavior_data    2.779334e-01 3.516843e-01 -2.505427e-02 3.417186e-01 2.016403e-02 1.287715e+00 -2.500000e+00 
behavior_data    1.223508e+00 3.826228e-01 1.302176e-02 8.883582e-01 -6.402239e-02 9.962701e-01 3.450000e+01 
behavior_data    3.479100e+00 3.126571e-01 5.715040e-03 9.960136e-01 -1.728765e-01 9.280376e-01 1.350000e+01 
behavior_data    -5.422934e-01 3.386585e-01 6.314701e-02 1.002073e+00 -8.074585e-03 1.186663e+00 4.500000e+00 
behavior_data    -1.849363e+00 3.801999e-01 2.093793e-02 1.005919e+00 -1.378402e-01 1.041423e+00 -8.500000e+00 
behavior_data    6.497470e-01 3.954118e-01 1.151994e-01 8.185668e-01 6.326585e-03 1.157698e+00 -8.500000e+00 
behavior_data    -4.948458e-01 2.391183e-01 -1.331197e-02 1.687785e+00 -2.713905e-03 8.834705e-01 -1.100000e+01 
behavior_data    9.015076e-01 2.778016e-01 5.030144e-02 1.548252e+00 -2.437744e-01 7.492316e-01 3.000000e+00 
behavior_data    7.661749e-02 4.055668e-01 4.059147e-02 9.932412e-01 -2.969519e-02 1.252530e+00 1.050000e+01 
behavior_data    9.998704e-01 3.129957e-01 1.332079e-02 1.199042e+00 -1.489821e-01 8.598564e-01 -5.000000e-01 
behavior_data    -1.118426e+00 3.628771e-01 8.409891e-02 1.456048e+00 -8.321402e-02 7.038124e-01 -5.500000e+00 
behavior_data    9.950207e-02 3.759788e-01 5.509374e-02 5.535320e-01 -1.851422e-02 1.158064e+00 5.000000e-01 
behavior_data    1.673038e-01 2.628363e-01 8.930385e-02 1.745176e+00 -2.164789e-01 8.965950e-01 -1.750000e+01 
behavior_data    -4.586128e-02 3.352566e-01 6.754743e-02 3.848808e-01 -4.209544e-02 1.014846e+00 3.500000e+00 
behavior_data    4.751825e-02 3.288998e-01 -3.777700e-04 5.968728e-01 2.802101e-02 1.118220e+00 -1.821506e-14 
behavior_data    -3.211007e-01 2.756080e-01 -2.890756e-02 1.166675e+00 -4.178432e-02 1.390092e+00 -6.500000e+00 
behavior_data    -1.633345e+00 3.610858e-01 -4.809524e-03 -5.840632e-02 1.396272e-01 8.192339e-01 -6.000000e+00 
behavior_data    -4.112971e-01 3.247615e-01 -2.785344e-02 7.866128e-01 9.841516e-03 1.286747e+00 -2.300000e+01 
behavior_data    -4.354896e-01 3.213777e-01 5.448201e-02 5.607570e-01 2.088502e-03 1.207145e+00 -5.000000e+00 
behavior_data    -5.729036e-01 2.683802e-01 4.723297e-02 1.343008e+00 -5.855348e-02 1.188642e+00 -6.500000e+00 
behavior_data    -6.939488e-01 3.117425e-01 3.082220e-02 1.392884e+00 -1.810007e-01 8.354638e-01 -1.000000e+00 
behavior_data    1.696870e+00 3.705724e-01 -1.800822e-03 2.795833e-01 4.949537e-02 1.041803e+00 7.000000e+00 
behavior_data    -1.382553e+00 3.765017e-01 2.120998e-02 6.303158e-03 -1.148969e-01 1.011770e+00 4.000000e+00 
behavior_data    -1.349169e+00 3.662609e-01 -3.071834e-02 8.822918e-01 -2.514870e-01 1.051206e+00 -1.000000e+01 
behavior_data    -3.181940e-01 2.746455e-01 -9.820397e-03 7.066693e-01 5.509513e-02 1.133549e+00 3.000000e+00 
behavior_data    -1.401225e+00 3.972845e-01 1.661247e-02 8.585351e-01 -1.796801e-01 1.135546e+00 -4.000000e+00 
behavior_data    -6.259619e-01 2.684089e-01 -4.660618e-03 7.690000e-01 -1.704560e-01 8.573325e-01 -5.000000e+00 
behavior_data    -2.519284e+00 3.596192e-01 1.021171e-02 1.147807e+00 -1.791776e-01 1.143612e+00 -2.000000e+01 
behavior_data    -1.354782e+00 3.241174e-01 -4.752052e-02 1.354300e+00 -1.431114e-01 9.510723e-01 -1.250000e+01 
behavior_data    3.834514e-01 4.069457e-01 8.113745e-03 1.287747e+00 5.947376e-02 9.922172e-01 -1.400000e+01 
behavior_data    3.114124e-01 2.974601e-01 5.383360e-02 1.890561e+00 -2.234773e-01 9.753407e-01 1.700000e+01 
behavior_data    -1.048375e+00 3.195032e-01 2.301651e-02 8.222769e-01 -9.794117e-02 1.072973e+00 7.000000e+00 
behavior_data    1.209745e+00 3.152070e-01 1.294155e-02 4.692124e-01 1.683730e-01 1.085802e+00 -6.000000e+00 

% ... following above pattern for more groups

	%%%%%%%%%%%%%%%%%%%%%%%
	%  Behavior Data End  %
	%%%%%%%%%%%%%%%%%%%%%%%

%------------------------------------------------------------------------

	%%%%%%%%%%%%%%%%%%%%%%%%%
	%  Behavior Name Start  %
	%%%%%%%%%%%%%%%%%%%%%%%%%

%  Numbers of Behavior Name should match the Behavior Data above

behavior_name AMF NDT1 NDTlin234 Drift1 Driftlin234 Thresh1 Dimlin234

