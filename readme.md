## PLS overview

- Preproc v2
- Mean BOLD, SPM inputs

---

For these condition-wise data we use the following strategy:

- Condition info (when during which run) will be encoded in mat info files.
- These 'dummy files' will correspond to the input structures that the pls GUI expects.
- Relevant fMRI data will be replaced with the desired inputs (e.g., SD BOLD, 1st level SPM data, VarTbx data, etc ...).
- The most recent version of the PLS toolbox (which allows for .mat inputs) can be cloned from <https://git.mpib-berlin.mpg.de/LNDG/pls>.

**S0_collectInputData:**

- unzip preprocessed data to the input directory

**S1_createTaskInfoMats:**

- load a PLS mat template manually before running the function
- insert the relevant condition information from regressors to create the datamat in PLS (i.e. block onsets & durations)
- create individual input files to PLS (.mat): `<ID>_task_PLS_info.mat`

*Note: To get a balanced matrix of condition onsets etc. -1 is entered for conditions that were not present for the run. Note that PLS give a warning for those, but will automatically drop them from processing.*

**S2_make_meanbold_datamat_task:**

- use PLS to create the mean matrices: `task_<ID>_BfMRIsessiondata.mat`
- these mean matrices are currently not used later, but serve as 'dummies' that fulfill all the specification requirements of the PLS toolbox
- *this step will throw a warning for all the ‘-1’ entries (see above)*

**S3a_commonGMvoxels_summary:**

- create 3D summary niftys from 4D input, e.g., SD BOLD
- note that these are not necessarily used in the pls pipeline if you choose to model other inputs such as spm inputs (see S11 here)

**S3b_commonGMvoxels_summary:**

- extract coordinates where ALL subjects had non-zero values in MNI-based grey matter mask
- loads 3D summary SD niftys as input to not re-load large 4D files

**S4_create_sdbold_STSWD_task_v6:**

- create condition-wise SD BOLD mats as input for PLS
- global means per block (across voxels) were normalized to 10, with individual images expressed with respect to the difference to this global mean. Data were then concatenated across blocks and runs within condition. Finally, BOLD SD of each voxel was calculated across the concatenated time series.

## Different versions

---

**v6**: Initial SD BOLD analysis using the v2 preprocessing data. Significant meancentered LV: stronger decrease in DMN dynamics with split attention. Behavioral PLS: stronger desynchronization is associated with better performers.

**v7**: For this analysis, volumes were exclusively extracted from volumes during stimulus presentation. The overall pattern of meancentered results did not change much, although the LV appeared more speckly. No significant LV was identified for predicting DDM and AMF though.