% S_0_collectInputData
% prepare tardis analysis; run locally
% This script copies the preprocessed files from the preproc directory to
% the input directory for the PLS analysis.

% 171218 | written by JQK for Merlin2 rest
% 180221 | function adapted from MerlinDynamics for STSWD YA rest
% 180228 | adapted for STSWD task (YA+OA)

pn.root1             = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
pn.root2             = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/';
pn.niipath          = [pn.root1, 'preproc/B_data/D_preproc/'];
pn.BOLDpath         = [pn.root1, 'analyses/B4_PLS_preproc2/B_data/BOLDin/']; mkdir(pn.BOLDpath); % path for all individual images; should be deleted after processing

%N = 44 YA + 53 OA;
IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

runs = {'run-1'; 'run-2'; 'run-3'; 'run-4'};

for indID = 1:numel(IDs)
    for indRun = 1:numel(runs)
        ID = IDs{indID};
        fileName = [ID, '_',runs{indRun},'_feat_detrended_highpassed_denoised_t2nlreg_2009c_3mm.nii'];
        if exist([pn.niipath, ID, '/preproc2/',runs{indRun},'/',fileName])
            disp(['Copying ',ID,' Run ', num2str(indRun)]);
            %gunzip([pn.niipath, ID, '/preproc2/',runs{indRun},'/',fileName], [pn.BOLDpath]);
            copyfile([pn.niipath, ID, '/preproc2/',runs{indRun},'/',fileName], [pn.BOLDpath]);
        else
            warning([ID,' Run ', num2str(indRun), ' not available']);
        end
    end
end

% copy 1126 nii

IDs = {'1126'};
for indID = 1:numel(IDs)
    for indRun = 1:numel(runs)
        ID = IDs{indID};
        fileName = [ID, '_',runs{indRun},'_feat_detrended_highpassed_denoised_nlreg_2009c_3mm.nii'];
        if exist([pn.niipath, ID, '/preproc2/',runs{indRun},'/',fileName])
            disp(['Copying ',ID,' Run ', num2str(indRun)]);
            %gunzip([pn.niipath, ID, '/preproc2/',runs{indRun},'/',fileName], [pn.BOLDpath]);
            copyfile([pn.niipath, ID, '/preproc2/',runs{indRun},'/',fileName], [pn.BOLDpath]);
        else
            warning([ID,' Run ', num2str(indRun), ' not available']);
        end
    end
end