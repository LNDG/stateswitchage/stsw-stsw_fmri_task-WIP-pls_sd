% Create info file for PLS analysis

% for ER-behavioral PLS: include AMF, relevant DDM parameters

clear all; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.PLSfiles = fullfile(rootpath, 'data', 'pls');
pn.summary = fullfile(rootpath, '..', '..', 'stsw_multimodal', 'data');

%% get summary data

load(fullfile(pn.summary, 'STSWD_summary_YAOA.mat'), 'STSWD_summary')

%% YA

IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

numSubs = numel(IDs);

dataForPLS = [];
for indCond = 1
    for indID = 1:numel(IDs)
        disp(['Processing ID ', IDs{indID}])
        groupfiles{indID,1} = ['SD_',IDs{indID}, '_v6_BfMRIsessiondata.mat'];
        curID = find(strcmp(STSWD_summary.IDs, IDs{indID}));
        dataForPLS{(indCond-1)*numSubs+indID,1} = STSWD_summary.HDDM_vat.driftEEGMRI(curID,1);
        dataForPLS{(indCond-1)*numSubs+indID,2} = STSWD_summary.HDDM_vat.driftEEGMRI_linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,3} = STSWD_summary.HDDM_vat.thresholdEEGMRI(curID,1);
        dataForPLS{(indCond-1)*numSubs+indID,4} = STSWD_summary.HDDM_vat.thresholdEEGMRI_linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,5} = STSWD_summary.HDDM_vat.nondecisionEEGMRI(curID,1);
        dataForPLS{(indCond-1)*numSubs+indID,6} = STSWD_summary.HDDM_vat.nondecisionEEGMRI_linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,7} = STSWD_summary.excitability_LV1.linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,8} = STSWD_summary.tfr_theta_LV1.linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,9} = STSWD_summary.tfr_gamma_LV1.linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,10} = STSWD_summary.tfr_prestim_alpha_LV1.linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,11} = STSWD_summary.pupil_LV1.linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,12} = STSWD_summary.CPPslopes.linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,13} = STSWD_summary.BetaSlope.linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,14} = STSWD_summary.CPPthreshold.linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,15} = STSWD_summary.BetaThreshold.linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,16} = STSWD_summary.SSVEPmag_norm.linear_win(curID);
        dataname{(indCond-1)*numSubs+indID,1} = 'behavior_data';
    end
end
groupfiles = groupfiles(find(~cellfun(@isempty,groupfiles)));

% normalize each variable: convert to z-scores
%dataForPLS_z = [dataForPLS(:,1), num2cell(zscore(cell2mat(dataForPLS(:,2:end))))];

Fillin.GROUPFILES = groupfiles';
Fillin.DATANAME = dataname;
Fillin.VALUE = dataForPLS;

% Output necessary information, has to be manually copied into .txt file

clc;
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES{indSub});
end

clc;
for indSub = 1:size(Fillin.VALUE,1) 
    fprintf('%s    %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d \n', Fillin.DATANAME{indSub}, Fillin.VALUE{indSub,:});
end


behavior_name ={'drift', 'driftchange', 'threshold', 'thresholdchange', ...
    'ndt', 'ndtchange', 'excitability', 'theta', 'gamma', 'prestimalpha', ...
    'pupil', 'cpp', 'betaslope', 'cppthresh', 'betathresh', 'ssvep'};

clc;
fprintf('%s    %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s \n', 'behavior_name', behavior_name{:});

% figure;imagesc(cell2mat(dataForPLS_z(:,2:end)))
% figure;imagesc(corrcoef(cell2mat(dataForPLS_z(:,2:end))))
% 
% [lv, coeff] = pca(cell2mat(dataForPLS_z(:,2:end)));
% figure; imagesc(lv)

%% OA

IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

numSubs = numel(IDs);
dataForPLS = [];
for indCond = 1
    for indID = 1:numel(IDs)
        disp(['Processing ID ', IDs{indID}])
        groupfiles{indID,1} = ['SD_',IDs{indID}, '_v6_BfMRIsessiondata.mat'];
        curID = find(strcmp(STSWD_summary.IDs, IDs{indID}));
        dataForPLS{(indCond-1)*numSubs+indID,1} = STSWD_summary.HDDM_vat.driftEEGMRI(curID,1);
        dataForPLS{(indCond-1)*numSubs+indID,2} = STSWD_summary.HDDM_vat.driftEEGMRI_linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,3} = STSWD_summary.HDDM_vat.thresholdEEGMRI(curID,1);
        dataForPLS{(indCond-1)*numSubs+indID,4} = STSWD_summary.HDDM_vat.thresholdEEGMRI_linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,5} = STSWD_summary.HDDM_vat.nondecisionEEGMRI(curID,1);
        dataForPLS{(indCond-1)*numSubs+indID,6} = STSWD_summary.HDDM_vat.nondecisionEEGMRI_linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,7} = STSWD_summary.excitability_LV1.linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,8} = STSWD_summary.tfr_theta_LV1.linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,9} = STSWD_summary.tfr_gamma_LV1.linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,10} = STSWD_summary.tfr_prestim_alpha_LV1.linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,11} = STSWD_summary.pupil_LV1.linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,12} = STSWD_summary.CPPslopes.linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,13} = STSWD_summary.BetaSlope.linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,14} = STSWD_summary.CPPthreshold.linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,15} = STSWD_summary.BetaThreshold.linear_win(curID);
        dataForPLS{(indCond-1)*numSubs+indID,16} = STSWD_summary.SSVEPmag_norm.linear_win(curID);
        dataname{(indCond-1)*numSubs+indID,1} = 'behavior_data';
    end
end
groupfiles = groupfiles(find(~cellfun(@isempty,groupfiles)));

% normalize each variable: convert to z-scores
%dataForPLS_z = [dataForPLS(:,1), num2cell(zscore(cell2mat(dataForPLS(:,2:end))))];

Fillin.GROUPFILES = groupfiles';
Fillin.DATANAME = dataname;
Fillin.VALUE = dataForPLS;

% Output necessary information, has to be manually copied into .txt file

clc;
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES{indSub});
end

clc;
for indSub = 1:size(Fillin.VALUE,1) 
    fprintf('%s    %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d \n', Fillin.DATANAME{indSub}, Fillin.VALUE{indSub,:});
end

behavior_name ={'drift', 'driftchange', 'threshold', 'thresholdchange', ...
    'ndt', 'ndtchange', 'excitability', 'theta', 'gamma', 'prestimalpha', ...
    'pupil', 'cpp', 'betaslope', 'cppthresh', 'betathresh', 'ssvep'};

clc;
fprintf('%s    %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s \n', 'behavior_name', behavior_name{:});


% figure;imagesc(cell2mat(dataForPLS_z(:,2:end)))
% figure;imagesc(corrcoef(cell2mat(dataForPLS_z(:,2:end))))
% 
% [lv, coeff] = pca(cell2mat(dataForPLS_z(:,2:end)));
% figure; imagesc(lv)


