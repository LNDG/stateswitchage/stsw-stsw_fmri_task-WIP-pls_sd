currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.PLSfiles = fullfile(rootpath, 'data', 'SD_STSWD_task_v2');
pn.plstoolbox = fullfile(rootpath, 'tools', 'pls'); addpath(genpath(pn.plstoolbox));

cd(pn.PLSfiles);

batch_plsgui('behavPLS_STSWD_SD_YA_OA_Paper2_3mm_1000P1000B_BfMRIanalysis.txt')

plsgui
